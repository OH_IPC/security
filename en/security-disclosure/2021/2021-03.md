## Security Vulnerabilities in March 2021


| Vulnerability ID | related Vulnerability | Vulnerability Descripton | affected versions | affected projects| fix link | reference |
| -------- |-------- | -------- | ----------- | ----------- | -------- | ------- |
|OpenHarmony-SA-2021-0301 | CVE-2021-22294| A component API of the HarmonyOS 2.0 has a permission bypass vulnerability.|OpenHarmony-1.0|distributedschedule_services_samgr_lite|   [链接](https://gitee.com/openharmony/distributedschedule_samgr_lite/pulls/7/files) |[链接](https://nvd.nist.gov/vuln/detail/CVE-2021-22294)|
|OpenHarmony-SA-2021-0302 | CVE-2021-22296| A component of HarmonyOS 2.0 has a DoS vulnerability. |OpenHarmony-1.0|kernel_liteos_a|   [链接](https://gitee.com/openharmony/kernel_liteos_a/pulls/48/files)|[链接](https://nvd.nist.gov/vuln/detail/CVE-2021-22296)|
